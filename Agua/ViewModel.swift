//
//  ViewModel.swift
//  Agua
//
//  Created by Dev on 02/09/2016.
//
//

class ViewModel {
    fileprivate let model: Model
    
    init() {
        model = Model()
    }
    
    func buttonTapped(buttonIdentifier: Int) {
        switch buttonIdentifier {
            case 0:
                model.addWater(amount: Constants.glassCapacity / 4)
            case 1:
                model.addWater(amount: Constants.glassCapacity / 2)
            case 2:
                model.addWater(amount: Constants.glassCapacity)
            default:
                break
        }
    }
    
    func summaryText() -> String {
        let amount = totalAmountOfWaterConsumed()
        return "You drank \(amount) ml of water"
    }
}

// MARK: Private

extension ViewModel {
    fileprivate func totalAmountOfWaterConsumed() -> Float {
        return model.amountOfWater
    }
}
