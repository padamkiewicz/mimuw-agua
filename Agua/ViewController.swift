//
//  ViewController.swift
//  Agua
//
//  Created by Dev on 02/09/2016.
//
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var quarterGlassButton: UIButton!
    @IBOutlet weak var halfGlassButton: UIButton!
    @IBOutlet weak var wholeGlassButton: UIButton!
    
    private let viewModel: ViewModel = ViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        quarterGlassButton.tag = 0
        halfGlassButton.tag = 1
        wholeGlassButton.tag = 2
        
        quarterGlassButton.addTarget(self, action: #selector(ViewController.buttonTapped(button:)), for: UIControlEvents.touchUpInside)
        halfGlassButton.addTarget(self, action: #selector(ViewController.buttonTapped(button:)), for: UIControlEvents.touchUpInside)
        wholeGlassButton.addTarget(self, action: #selector(ViewController.buttonTapped(button:)), for: UIControlEvents.touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateSummaryLabel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func buttonTapped(button: UIButton) {
        viewModel.buttonTapped(buttonIdentifier: button.tag)
        updateSummaryLabel()
    }
    
    func updateSummaryLabel() {
        summaryLabel.text = viewModel.summaryText()
    }
}

