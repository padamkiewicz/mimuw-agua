//
//  Model.swift
//  Agua
//
//  Created by Dev on 02/09/2016.
//
//

class Model {
    private(set) var amountOfWater: Float
    
    init() {
        amountOfWater = 0.0
    }
    
    func addWater(amount: Float) {
        amountOfWater += amount
    }
}
